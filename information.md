* Adresse : 352 Cours Emile Zola, 69100 Villeurbanne, France

* ### Pauline ROUILLET

  Chargée de promotion

  [prouillet@simplon.co](mailto:prouillet@simplon.co)

* ### Ingrid GENET

  Chargée de promotion

  [igenet@simplon.co](mailto:igenet@simplon.co)

* ### Pierre-Yves CHARPENET

  Directeur Simplon Auvergne Rhône-Alpes

  [pycharpenet@simplon.co](mailto:pycharpenet@simplon.co)



* Github : https://github.com/simplon-lyon
* twitter : https://twitter.com/SimplonLyon
* LinkedIn : https://www.linkedin.com/company/simplon-co/
* Facebook : https://www.facebook.com/SimplonLyon/?ref=br_rs